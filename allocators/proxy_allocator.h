#pragma once
#include "../memory.h"

// (C) 2013 - Monkeykingz Audio
// See LICENSE for more information.




namespace foundation
{
    /// ProxyAllocator is a thin wrapper around any parent allocator
    /// designed to be used as a scoped memory allocation checker.
    ///
    ///     void foo( Allocator& a )
    ///     {
    ///         ProxyAllocator tmp_allocator( parent_allocator, "Foo temporaries" );
    ///         // ... do something
    ///         // And tmp_allocator checks that all memory allocated by
    ///         // him is deallocated.
    ///     }
    class ProxyAllocator : public Allocator
    {
    public:
        ProxyAllocator( Allocator& parent, const char* name );
        virtual ~ProxyAllocator();

        virtual void* allocate( uint32_t size, uint32_t align = DEFAULT_ALIGN );
        virtual void deallocate( void* p );
        virtual uint32_t allocated_size( void* p );
        virtual uint32_t total_allocated();

        //////////////////////////////////////////////////////////////////////////

        const char* name;
    private:
        Allocator& allocator;

        uint32_t allocated;
        uint32_t deallocated;
    };

}
