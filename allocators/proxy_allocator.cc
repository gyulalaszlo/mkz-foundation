#include "proxy_allocator.h"

// (C) 2013 - Monkeykingz Audio
// See LICENSE for more information.

#include <new>
#include <assert.h>

#include "foundation/memory.h"

using namespace foundation;

namespace foundation
{

    ProxyAllocator::ProxyAllocator( Allocator& parent, const char* name )
        : name( name )
        , allocator( parent )
        , allocated(0)
        , deallocated(0)
    {

    }

    ProxyAllocator::~ProxyAllocator()
    {
        assert( allocated == deallocated );
    }

    void* ProxyAllocator::allocate( uint32_t size, uint32_t align )
    {
        void* allocated_ptr = allocator.allocate( size, align );
        allocated += allocator.allocated_size( allocated_ptr );
        return allocated_ptr;
    }

    void ProxyAllocator::deallocate( void* p )
    {
        if (p != nullptr)
        {
          deallocated += allocator.allocated_size( p );
        }
        allocator.deallocate( p );
    }

    uint32_t ProxyAllocator::allocated_size( void* p )
    {
        return allocator.allocated_size( p );
    }

    uint32_t ProxyAllocator::total_allocated()
    {
        return allocator.total_allocated();
    }

}
