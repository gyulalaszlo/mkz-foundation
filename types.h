#pragma once

#include <stdint.h>

// Using alignof and nullptr as macro is error in VS 2012.
#define _ALLOW_KEYWORD_MACROS

#ifndef alignof
#define alignof(x) __alignof(x)
#endif

