#pragma once
#include "hash.h"
namespace foundation
{
    namespace hash
    {
        /// Remove all elements from the hash
        template<typename T> void clear( Hash<T>& hsh );

        /// Get the number of elements in hsh
        template<typename T> uint32_t size( const Hash<T>& hsh );
        
        /// Get a pointer from the hash or nullptr if no such ptr is there.
        /// This is a convinience function to get rid of casting when nullptr is planned to be
        /// returned as a default value.
        template<typename T> T* get_ptr( const Hash<T*>& hsh, uint64_t key );

        /// Deallocate all pointed objects in the hash with allocator.deallocate()
        /// Useful for hashes containing allocated pointers.
        template <typename T>
        void deallocate_values( Hash<T>& hsh, Allocator& allocator );

        /// Extract data from the given flat array of structures.
        template <typename KeyType, typename T, typename Data>
        void extract_from( Hash<T>& hsh, const Data* start, const Data* end, const uint32_t key_offset, const uint32_t data_offset );

        /// A convinience accessor for collections with sizes
        template <typename KeyType, typename T, typename Data>
        void extract_from( Hash<T>& hsh, const Data* start, const int data_size, const uint32_t key_offset, const uint32_t data_offset );


        //////////////////////////////////////////////////////////////////////////
        // Object (pointer) functions.
        // These functions deal with collection of (pointer) objects.

        /// Destroy all pointed objects in the hash with MAKE_DELETE.
        /// Useful for hashes containing allocated objects with (virtual) destructors.
        template <typename T>
        inline void destroy_objects( Hash<T*>& hsh, Allocator& allocator );


        /// Collect a collection of objects into the hash by using the value at @a key_offset.
        template <typename KeyType, typename T>
        void collect_from( Hash<T*>& hsh, T* start, T*  end, const uint32_t key_offset );


        /// Collect a collection of objects into the hash by using the value at @a key_offset.
        template <typename KeyType, typename T>
        void collect_from( Hash<T*>& hsh, T* start, const int data_size, const uint32_t key_offset );




    }

    //////////////////////////////////////////////////////////////////////////

    namespace hash
    {
        template<typename T>
        inline void clear( Hash<T>& hsh )
        {
            array::clear( hsh._data );
            array::clear( hsh._hash );
        }

        template<typename T> 
        inline uint32_t size( const Hash<T*>& hsh )
        {
            return array::size( hsh._data );
        }


        template<typename T> 
        inline T* get_ptr( const Hash<T*>& hsh, uint64_t key )
        {
            return hash::get( hsh, key, ( T* )nullptr );
        }


        template <typename T>
        inline void deallocate_values( Hash<T>& hsh, Allocator& allocator )
        {
            for ( const typename Hash<T>::Entry* it = hash::begin( hsh );
                    it != hash::end( hsh ); ++it )
            {
                allocator.deallocate( it->value );
            }
        }


        template <typename T>
        inline void destroy_objects( Hash<T*>& hsh, Allocator& allocator )
        {
            for ( const typename Hash<T*>::Entry* it = hash::begin( hsh );
                    it != hash::end( hsh ); ++it )
            {
                MAKE_DELETE( allocator, T, it->value );
            }
        }

        template <typename KeyType, typename T, typename Data>
        inline void extract_from( Hash<T>& hsh, Data* start, Data* end, const uint32_t key_offset, const uint32_t data_offset )
        {
            for ( const Data* cursor = start; cursor != end; ++cursor )
            {
                KeyType key = *( KeyType* ) memory::pointer_add( cursor, key_offset );
                T val = *( T* ) memory::pointer_add( cursor, data_offset );
                hash::set( hsh, key, val );
            }
        }

        template <typename KeyType, typename T, typename Data>
        inline void extract_from( Hash<T>& hsh, Data* start, const int data_size, const uint32_t key_offset, const uint32_t data_offset )
        {
            extract_from<KeyType>( hsh, start, start + data_size, key_offset, data_offset );
        }

        //////////////////////////////////////////////////////////////////////////


        template <typename KeyType, typename T>
        inline void collect_from( Hash<T*>& hsh, T* start, T*  end, const uint32_t key_offset )
        {
            for ( T* cursor = start; cursor != end; ++cursor )
            {
                KeyType key = *( KeyType* ) memory::pointer_add( cursor, key_offset );
                hash::set( hsh, key, cursor );
            }
        }

        template <typename KeyType, typename T>
        inline void collect_from( Hash<T*>& hsh, T* start, const int data_size, const uint32_t key_offset )
        {
            collect_from<KeyType>( hsh, start, start + data_size, key_offset );
        }

    }
}
