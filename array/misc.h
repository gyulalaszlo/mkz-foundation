#pragma once
#include "../array.h"

// (C) 2013 - Monkeykingz Audio
// See LICENSE for more information.



namespace foundation
{

    namespace array
    {

        /// Get the first element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        T& first( Array<T>& a, T& default_value );

        /// Get the last element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        T& last( Array<T>& a, T& default_value );

        /// Get the first element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        const T& first( const Array<T>& a, const T& default_value );

        /// Get the last element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        const T& last( const Array<T>& a, const T& default_value );

        //////////////////////////////////////////////////////////////////////////

        /// Get the used size of the array
        template <typename T> 
        const uint32_t size_in_chars( const Array<T>& a );

        /// Resize to new_size and set the array contents to the specified fill_value
        template <typename T> 
        void resize_and_clear( Array<T>& a, uint32_t new_size, const T& fill_value );

        /// Append another array to the end of this array. Returns the index of the
        /// first element of the copied elements.
        template <typename T> 
        uint32_t append_array( Array<T>& a, uint32_t count, const T* soruce_values );

        /// Enlarge the array by count and return the index of the 
        /// first element of the newly allocated ones.
        template <typename T> 
        uint32_t make_space( Array<T>& a, uint32_t count );

    }
}
namespace foundation
{

    namespace array
    {

        /// Get the first element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        T& first( Array<T>& a, T& default_value )
        {
            if (array::empty( a )) return default_value;
            return a[ 0 ];
        }

        /// Get the last element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        T& last( Array<T>& a, T& default_value )
        {
            if (array::empty( a )) return default_value;
            return a[ array::size( a ) - 1 ];
        }

        /// Get the first element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        const T& first( const Array<T>& a, const T& default_value )
        {
            if (array::empty( a )) return default_value;
            return a[ 0 ];
        }

        /// Get the last element of the array (or default_value if
        /// the array is empty)
        template <typename T> 
        const T& last( const Array<T>& a, const T& default_value )
        {
            if (array::empty( a )) return default_value;
            return a[ array::size( a ) - 1 ];
        }

        //////////////////////////////////////////////////////////////////////////


        template <typename T> 
        inline const uint32_t size_in_chars( const Array<T>& a )
        {
            return a._size * sizeof(T);
        }

        template <typename T> 
        inline void resize_and_clear( Array<T>& a, uint32_t new_size, const T& fill_value )
        {
            array::resize( a, new_size );
            for(uint32_t i = 0; i < new_size; ++i) { a[i] = fill_value; }
        }

        template <typename T> 
        inline uint32_t append_array( Array<T>& a, uint32_t count, const T* soruce_values )
        {
            uint32_t existing_size = make_space( a, count );
            memcpy( &a[existing_size], soruce_values, count * sizeof(T) );
            return existing_size;
        }

        template <typename T> 
        inline uint32_t make_space( Array<T>& a, uint32_t count )
        {
            uint32_t existing_size = array::size( a );
            array::resize( a, existing_size + count );
            return existing_size;
        }
    }

}
